package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.repository.RoleRepository;

@Controller
@RequestMapping("/role/")
public class RoleController {

	@Autowired
	private RoleRepository roleRepository;
	
	//USING API//
	@GetMapping("indexapi")
	public ModelAndView getAllRole(){
		ModelAndView role = new ModelAndView("/role/indexapi");
		
		return role;
	}
	
}
