package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api/")
public class ApiVariantController {
	
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	VariantRepository variantRepository;
	
	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant(){
		
		try {
			
//			List<Variant> listVariant = this.variantRepository.findAll();
			List<Variant> listVariant = this.variantRepository.findByVariant();//belum dibikin
//			List<Variant> listVariant = this.variantRepository.findByIsActive(true);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		
		}
		catch(Exception e){
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		}
		
	}
	
	@PostMapping("variant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){
		variant.createBy = "admin1";
//		variant.setCreateBy("admin1");
		variant.createDate = new Date();
//		variant.setCreateDate(new Date());
		
		Variant variantData = this.variantRepository.save(variant);
		
		if(variantData.equals(variant)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("variant/{id}")
	public ResponseEntity<Object> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			return new ResponseEntity<>(variant,HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("variant/edit/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id, @RequestBody Variant variant){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		
		if(variantData.isPresent()) {
			variant.id = id;
			variant.modifyBy = "admin1";
			variant.modifyDate = new Date();
			variant.createBy = variantData.get().createBy;
			variant.createDate = variantData.get().createDate;
			
			this.variantRepository.save(variant);
			return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("variant/delete/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		
		if(variantData.isPresent()) {
			Variant variant = new Variant();
			variant.id = id;
//			category.isActive = false;
			variant.setIsActive(false);
			variant.modifyBy = "admin1";
			variant.modifyDate = new Date();
			variant.createBy = variantData.get().createBy;
			variant.createDate = variantData.get().createDate;
			variant.variantInitial = variantData.get().variantInitial;
			variant.variantName = variantData.get().variantName;
			variant.categoryId = variantData.get().categoryId;
			
			this.variantRepository.save(variant);
			return new ResponseEntity<>("Deleted Data Success",HttpStatus.OK);
			
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
}
