package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping("index")
	public String index() {
		return "index.html";
	}
	
	@GetMapping("home")
	public String home() {
		return "home.html";
	}
	
	@GetMapping("kalkulator")
	public String kalkulator() {
		return "kalkulator.html";
	}
}
