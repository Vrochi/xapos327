package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query(value = "SELECT r FROM Role r where r.isDelete = false order by roleName")//pakai java class
	List<Role> findByRole();
	
	
	//PAGING//
	@Query(value = "select * from tblrole where lower(role_name) like lower(concat('%',?1,'%')) and is_delete = ?2 order by role_name asc", nativeQuery = true)
	Page<Role> findByIsDelete(String keyword,  Boolean isDelete, Pageable page);
	
	@Query(value = "select * from tblrole where lower(role_name) like lower(concat('%',?1,'%')) and is_delete = ?2 order by role_name desc", nativeQuery = true)
	Page<Role> findByIsDeleteDESC(String keyword,  Boolean isDelete, Pageable page);
	
	
}
