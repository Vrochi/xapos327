create table order_header
(id bigint generated always as identity primary key not null,
reference varchar(255) not null,
amount double precision not null,
--amount decimal(18,4) not null,
is_active boolean not null,
create_by varchar(50) not null,
create_date timestamp not null,
modify_by varchar(50) null,
modify_date timestamp null)

select * from product

create table order_detail
(id bigint generated always as identity primary key not null,
header_id bigint not null,
product_id bigint not null,
quantity double precision not null,
price double precision not null,
is_active boolean not null,
create_by varchar(50) not null,
create_date timestamp not null,
modify_by varchar(50) null,
modify_date timestamp null)


alter table order_detail
add foreign key(header_id)
references order_header(id)

alter table order_detail
add foreign key (product_id)
references product(id)

select * from order_header

select * from order_detail

alter table category
add column sphoto varchar(255)

create table tblrole
(id bigint generated always as identity primary key not null,
role_name varchar(80) null,
is_delete boolean null,
created_by varchar(255) not null,
created_date timestamp not null,
updated_by varchar(255) null,
updated_date timestamp null)

select * from tblrole

select * from category
