package com.xsisacademy.pos.xsisacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos327WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Xapos327WebApplication.class, args);
	}

}
