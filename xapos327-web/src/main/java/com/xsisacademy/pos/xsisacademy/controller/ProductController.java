package com.xsisacademy.pos.xsisacademy.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/product/")
public class ProductController {
	
	public static String api_url_master="";
	
	public ProductController() {
		try {
			String configFilePath = "src/main/resources/config.properties";
			FileInputStream propsInput = new FileInputStream(configFilePath);
			Properties prop = new Properties();
			prop.load(propsInput);
			
			api_url_master = prop.getProperty("API_URL_MASTER");
		}
		catch(FileNotFoundException e) {
			api_url_master = "";
		}
		catch(Exception e){
			api_url_master = "";
		}
	}
	
	//USING API
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/product/indexapi.html");
		view.addObject("api_url_master", api_url_master);
		return view;
	}
	
}
