package com.xsisacademy.pos.xsisacademy.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;
	
	@Column(name= "variant_id")
	public Long variantId;
	
	@Column(name= "product_initial")
	public String productInitial;
	
	@Column(name= "product_name")
	public String productName;
	
	@Column(name= "description")
	public String description;
	
	@Column(name= "price")
	public double price;
	
	@Column(name= "stock")
	public double stock;

	@Column(name= "is_active")
	public Boolean isActive;
	
	@Column(name= "create_by")
	public String createBy;
	
	@Column(name= "create_date")
	public Date createDate;
	
	@Column(name= "modify_by")
	public String modifyBy;
	
	@Column(name= "modify_date")
	public Date modifyDate;
	
	@ManyToOne
	@JoinColumn(name = "variant_id", insertable= false, updatable= false)
	public Variant variant;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVariantId() {
		return variantId;
	}

	public void setVariantId(Long variantId) {
		this.variantId = variantId;
	}

	public String getProductInitial() {
		return productInitial;
	}

	public void setProductInitial(String productInitial) {
		this.productInitial = productInitial;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getStock() {
		return stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Variant getVariant() {
		return variant;
	}

	public void setVariant(Variant variant) {
		this.variant = variant;
	}
	
	
}
