package com.xsisacademy.pos.xsisacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos327MasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(Xapos327MasterApplication.class, args);
	}

}
