package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {

	@Query(value = "SELECT v FROM Variant v where v.isActive = true order by variantName")//pakai java class
	List<Variant> findByVariant();
	
	//menggunakan JpaRepository
	List<Variant> findByIsActive(Boolean isActive);
	
	@Query(value= "SELECT v FROM Variant v where v.isActive = true and v.categoryId = ?1 order by variantName")
	List<Variant> findByCategoryId(Long categoryId);
}
