package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

	//menggunakana JpaRepository
	List<Product> findByIsActive(Boolean isActive);
	
	//command di postgre tetep bisa dipake walaupun pake java class
	@Query("SELECT p FROM Product p where p.isActive = ?1 and p.productInitial = ?2 order by p.productName")// tanda tanya itu buat dapetin dari parameter(kalo parameter 1 ya 1 kalo parameter 2 ya 2 dan seterusnya
	List<Product> findByIsActiveandInitial(Boolean isActive, String productInitial);
	
	@Query("SELECT p FROM Product p where p.isActive = ?1 order by productName")
	List<Product> findAllProduct(Boolean isActive);
}
